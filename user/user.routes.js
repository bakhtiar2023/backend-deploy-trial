const express = require("express");
const userController = require("./user.controllers")
const userRoute = express.Router();

userRoute.get("/login", userController.getLogin);

module.exports = userRoute;
