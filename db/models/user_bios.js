'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_bios extends Model {
    static associate(models) {
      user_bios.belongsTo(models.user_games,{as:"user",foreignKey:"userId"})
    }
  }
  user_bios.init({
    userId: DataTypes.INTEGER,
    fullname: DataTypes.STRING,
    address: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    dateOfBirth: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_bios',
  });
  return user_bios;
};