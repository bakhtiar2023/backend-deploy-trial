const express = require("express");
const app = express();
const port = 3000;
const userRouter = require("./user/user.routes")

app.use(express.json())
app.get('/', (req, res) => {
    res.send('Hey this is my API running 🥳')
  })

app.use("/",userRouter)

app.listen(port, console.log(`listen port ${port}`))

module.exports=app